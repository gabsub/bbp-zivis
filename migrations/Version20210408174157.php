<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210408174157 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE resignation_request_approvers (id INT AUTO_INCREMENT NOT NULL, approver_id INT NOT NULL, insertor_id INT NOT NULL, role VARCHAR(255) NOT NULL, inserted_at DATETIME NOT NULL, INDEX IDX_188BE1FBBB23766C (approver_id), INDEX IDX_188BE1FB76056621 (insertor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE resignation_request_approvers ADD CONSTRAINT FK_188BE1FBBB23766C FOREIGN KEY (approver_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request_approvers ADD CONSTRAINT FK_188BE1FB76056621 FOREIGN KEY (insertor_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE resignation_request_approvers');
    }
}
