<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407134213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE invoice_refund_request (id INT AUTO_INCREMENT NOT NULL, submitter_id INT NOT NULL, approver_id INT DEFAULT NULL, created_at DATETIME NOT NULL, status INT NOT NULL, refund_amount DOUBLE PRECISION NOT NULL, invoice_file_name VARCHAR(255) NOT NULL, accepted_at DATETIME DEFAULT NULL, INDEX IDX_85A23BB7919E5513 (submitter_id), INDEX IDX_85A23BB7BB23766C (approver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE invoice_refund_request ADD CONSTRAINT FK_85A23BB7919E5513 FOREIGN KEY (submitter_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE invoice_refund_request ADD CONSTRAINT FK_85A23BB7BB23766C FOREIGN KEY (approver_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE invoice_refund_request');
    }
}
