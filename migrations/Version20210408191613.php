<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210408191613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE feedback_question (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, last_contributor_id INT NOT NULL, number INT NOT NULL, answer_type VARCHAR(255) NOT NULL, question VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, active_status TINYINT(1) NOT NULL, INDEX IDX_F9B9E23CF675F31B (author_id), INDEX IDX_F9B9E23C141EAAD3 (last_contributor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE feedback_question ADD CONSTRAINT FK_F9B9E23CF675F31B FOREIGN KEY (author_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE feedback_question ADD CONSTRAINT FK_F9B9E23C141EAAD3 FOREIGN KEY (last_contributor_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE feedback_question');
    }
}
