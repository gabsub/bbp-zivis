<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407113022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE vacation_request (id INT AUTO_INCREMENT NOT NULL, submitter_id INT NOT NULL, substitute_id INT NOT NULL, approver_id INT DEFAULT NULL, vacation_type_id INT NOT NULL, starts_at DATETIME NOT NULL, finishes_at DATETIME NOT NULL, created_at DATETIME NOT NULL, accepted_at DATETIME DEFAULT NULL, status INT NOT NULL, INDEX IDX_2A3500FC919E5513 (submitter_id), INDEX IDX_2A3500FC19ECAD51 (substitute_id), INDEX IDX_2A3500FCBB23766C (approver_id), INDEX IDX_2A3500FCD4EE03F0 (vacation_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vacation_request ADD CONSTRAINT FK_2A3500FC919E5513 FOREIGN KEY (submitter_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE vacation_request ADD CONSTRAINT FK_2A3500FC19ECAD51 FOREIGN KEY (substitute_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE vacation_request ADD CONSTRAINT FK_2A3500FCBB23766C FOREIGN KEY (approver_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE vacation_request ADD CONSTRAINT FK_2A3500FCD4EE03F0 FOREIGN KEY (vacation_type_id) REFERENCES vacation_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE vacation_request');
    }
}
