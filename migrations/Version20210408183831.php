<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210408183831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE resignation_request (id INT AUTO_INCREMENT NOT NULL, submitter_id INT NOT NULL, approver_manager_id INT DEFAULT NULL, approver_administrator_id INT DEFAULT NULL, approver_accountant_id INT DEFAULT NULL, approver_itadmin_id INT DEFAULT NULL, approver_lawyer_id INT DEFAULT NULL, submitted_at DATETIME NOT NULL, quitting_at DATETIME NOT NULL, status INT NOT NULL, approved_by_manager_at DATETIME DEFAULT NULL, approved_by_administrator_at DATETIME DEFAULT NULL, approved_by_accountant_at DATETIME DEFAULT NULL, approved_by_itadmin_at DATETIME DEFAULT NULL, approved_by_lawyer_at DATETIME DEFAULT NULL, status_by_manager INT DEFAULT NULL, status_by_administrator INT DEFAULT NULL, status_by_accountant INT DEFAULT NULL, status_by_itadmin INT DEFAULT NULL, status_by_lawyer INT DEFAULT NULL, INDEX IDX_D92EE44C919E5513 (submitter_id), INDEX IDX_D92EE44C3C386216 (approver_manager_id), INDEX IDX_D92EE44CF54D35F5 (approver_administrator_id), INDEX IDX_D92EE44CC23A5F6C (approver_accountant_id), INDEX IDX_D92EE44C2638DE19 (approver_itadmin_id), INDEX IDX_D92EE44C4E62FEF3 (approver_lawyer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44C919E5513 FOREIGN KEY (submitter_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44C3C386216 FOREIGN KEY (approver_manager_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44CF54D35F5 FOREIGN KEY (approver_administrator_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44CC23A5F6C FOREIGN KEY (approver_accountant_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44C2638DE19 FOREIGN KEY (approver_itadmin_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE resignation_request ADD CONSTRAINT FK_D92EE44C4E62FEF3 FOREIGN KEY (approver_lawyer_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE resignation_request');
    }
}
