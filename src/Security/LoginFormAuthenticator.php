<?php

namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use App\Repository\UserRepository;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator implements PasswordAuthenticatedInterface
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $csrfTokenManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function supports(Request $request)
    {
        return self::LOGIN_ROUTE === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];


        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        if (!$user) {

            if($this->checkAd($credentials['username'], $credentials['password'])){
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);
                return $user;
            }
            else{
               
                throw new CustomUserMessageAuthenticationException('Vartotojas su tokiu prisijungimo vardu nerastas.');
            }
        }

        return $user;
    }

    //TODO: pagalvot apie info parsitraukima kiekviena nakti
    public function checkCredentials($credentials, UserInterface $user)
    {
        //dump( $this->passwordEncoder->isPasswordValid($user, $credentials['password']));
        //dump($credentials);

        $userRepository = $this->entityManager->getRepository(User::class);

        $username =  $credentials['username'];
        $password =  $credentials['password'];

        $result = false;
        if(!defined('DOMAIN_FQDN')){
            define('DOMAIN_FQDN', 'zivisgroup.local');
        }
        if(!defined('LDAP_SERVER')){
            define('LDAP_SERVER', '192.168.1.17');
        }
        
        $user = strip_tags( $username) .'@'. DOMAIN_FQDN;
        $pass = stripslashes($password);
        $conn = \ldap_connect("ldap://". LDAP_SERVER ."/");
        if (!$conn){
            $err = 'Could not connect to LDAP server';
        }else{
            //define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);

            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($conn, $user, $pass);

            ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);

            if ($bind){
                $base_dn = array("DC=". join(',DC=', explode('.', DOMAIN_FQDN)), "OU=zivisgroup,DC=". join(',DC=', explode('.', DOMAIN_FQDN)));
                $result = ldap_search(array($conn,$conn), $base_dn, "(sAMAccountName=".$username.")");
                if (!count($result)){
                    $err = 'Unable to login: '. ldap_error($conn);
                }else{
                    foreach ($result as $res){
                        $info = ldap_get_entries($conn, $res);
                        $user = $userRepository->findOneBy(['username' => $username]);
                        if(array_key_exists('manager', $info['0'])){
                            $manager = explode(',', $info['0']['manager']['0']);
                            $manager = explode('=', $manager[0]);
                            $manager = explode(' ', $manager[1]);
                            $manager = $userRepository->findOneBy([ 'name' => $manager[0], 'lastname' => $manager[1]]);
                            $user->setManager($manager);
                        } //else {
                        //     $user->setManager(null);
                        // }
                        $user->setUsername($info['0']['samaccountname']['0']);
                        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$dUV4cjdhMFhZN2dpQUZLSA$EjWh1vMKyNS0idWPEa+5DQM1JMh3oG7qr1iH07dq2Sg');
                        $user->setEmail($info['0']['mail']['0']);
                        $user->setName($info['0']['givenname']['0']);
                        $user->setLastname($info['0']['sn']['0']);
                        $user->setPosition($info['0']['title']['0']);
                        $user->setCompany($info['0']['company']['0']);
                        $user->setAddress($info['0']['streetaddress']['0'] . ' ' . $info['0']['postofficebox']['0'] . ', ' . $info['0']['l']['0'] . ', ' . $info['0']['co']['0'] . ', ' . $info['0']['c']['0'] . '-' . $info['0']['postalcode']['0']);
                        $user->setPhone($info['0']['telephonenumber']['0']);
                        //$dateOfEmployment = new \DateTime(substr($info['0']['whencreated']['0'], 0, -3));
                        //$user->setDateOfEmployment($dateOfEmployment);
                        $this->entityManager->persist($user);
                        $this->entityManager->flush();
                    }
                }
            }
        }

        if($result !== false){
            return true;
        }
        else{
            return false;
        }


        //return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey)
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
            return new RedirectResponse($targetPath);
        }

        // For example : return new RedirectResponse($this->urlGenerator->generate('some_route'));
        //throw new \Exception('TODO: provide a valid redirect inside '.__FILE__);
        return new RedirectResponse($this->urlGenerator->generate('main'));
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    public function checkAd($username, $password){
        $result = false;
        $userRepository = $this->entityManager->getRepository(User::class);

        define('DOMAIN_FQDN', 'zivisgroup.local');
        define('LDAP_SERVER', '192.168.1.17');
        $user = strip_tags( $username) .'@'. DOMAIN_FQDN;
        $pass = stripslashes($password);
        $conn = \ldap_connect("ldap://". LDAP_SERVER ."/");
        if (!$conn){
            $err = 'Could not connect to LDAP server';
        }else{
            //define('LDAP_OPT_DIAGNOSTIC_MESSAGE', 0x0032);

            ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($conn, LDAP_OPT_REFERRALS, 0);

            $bind = @ldap_bind($conn, $user, $pass);

            ldap_get_option($conn, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);

            if ($bind){
                $base_dn = array("DC=". join(',DC=', explode('.', DOMAIN_FQDN)), "OU=zivisgroup,DC=". join(',DC=', explode('.', DOMAIN_FQDN)));
                $result = ldap_search(array($conn,$conn), $base_dn, "(sAMAccountName=".$username.")");
                if (!count($result)){
                    $err = 'Unable to login: '. ldap_error($conn);
                }else{
                    foreach ($result as $res){
                        $info = ldap_get_entries($conn, $res);
                        if(!$userRepository->findOneBy([ 'username' => $info['0']['samaccountname']['0']])){
                            $user = new User();
                            if(array_key_exists('manager', $info['0'])){
                                $manager = explode(',', $info['0']['manager']['0']);
                                $manager = explode('=', $manager[0]);
                                $manager = explode(' ', $manager[1]);
                                $manager = $userRepository->findOneBy([ 'name' => $manager[0], 'lastname' => $manager[1]]);
                                $user->setManager($manager);
                            }
                            $user->setUsername($info['0']['samaccountname']['0']);
                            $user->setRoles(["ROLE_USER"]);
                            $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$dUV4cjdhMFhZN2dpQUZLSA$EjWh1vMKyNS0idWPEa+5DQM1JMh3oG7qr1iH07dq2Sg');
                            $user->setEmail($info['0']['mail']['0']);
                            $user->setName($info['0']['givenname']['0']);
                            $user->setLastname($info['0']['sn']['0']);
                            $user->setPosition($info['0']['title']['0']);
                            $user->setCompany($info['0']['company']['0']);
                            $user->setAddress($info['0']['streetaddress']['0'] . '-' . $info['0']['postofficebox']['0'] . ', ' . $info['0']['l']['0'] . ', ' . $info['0']['co']['0'] . ', ' . $info['0']['c']['0'] . '-' . $info['0']['postalcode']['0']);
                            $user->setPhone($info['0']['telephonenumber']['0']);
                            //$dateOfEmployment = new \DateTime(substr($info['0']['whencreated']['0'], 0, -3));
                            //$user->setDateOfEmployment($dateOfEmployment);
                            $this->entityManager->persist($user);
                            $this->entityManager->flush();
                        }
                    }
                }
            }
        }

        if($result !== false){
            return true;
        }
        else{
            return false;
        }

    }
    
}
