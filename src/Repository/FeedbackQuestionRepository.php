<?php

namespace App\Repository;

use App\Entity\FeedbackQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FeedbackQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeedbackQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeedbackQuestion[]    findAll()
 * @method FeedbackQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeedbackQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FeedbackQuestion::class);
    }

    // /**
    //  * @return FeedbackQuestion[] Returns an array of FeedbackQuestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FeedbackQuestion
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findMaxNumber(): ?FeedbackQuestion
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.number', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findOneByNumber($value): ?FeedbackQuestion
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.number = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return FeedbackQuestion[] Returns an array of FeedbackQuestion objects
     */
    public function findActiveQuestions()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.activeStatus = true')
            ->orderBy('f.number', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return FeedbackQuestion[] Returns an array of FeedbackQuestion objects
     */
    public function findNotActiveQuestions()
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.activeStatus = false')
            ->orderBy('f.number', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
