<?php

namespace App\Repository;

use App\Entity\ResignationRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResignationRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResignationRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResignationRequest[]    findAll()
 * @method ResignationRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResignationRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResignationRequest::class);
    }

    // /**
    //  * @return ResignationRequest[] Returns an array of ResignationRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResignationRequest
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return ResignationRequest[] Returns an array of ResignationRequest objects
     */
    public function findBySubmitter($id)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.submitter = :val')
            ->setParameter('val', $id)
            ->orderBy('r.submittedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
