<?php

namespace App\Repository;

use App\Entity\InvoiceRefundRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InvoiceRefundRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceRefundRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceRefundRequest[]    findAll()
 * @method InvoiceRefundRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRefundRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvoiceRefundRequest::class);
    }

    // /**
    //  * @return InvoiceRefundRequest[] Returns an array of InvoiceRefundRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InvoiceRefundRequest
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return InvoiceRefundRequest[] Returns an array of InvoiceRefundRequest objects
     */
    public function findBySubmitter($id)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.submitter = :val')
            ->setParameter('val', $id)
            ->orderBy('i.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
