<?php

namespace App\Repository;

use App\Entity\ResignationRequestApprovers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ResignationRequestApprovers|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResignationRequestApprovers|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResignationRequestApprovers[]    findAll()
 * @method ResignationRequestApprovers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResignationRequestApproversRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResignationRequestApprovers::class);
    }

    // /**
    //  * @return ResignationRequestApprovers[] Returns an array of ResignationRequestApprovers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResignationRequestApprovers
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
