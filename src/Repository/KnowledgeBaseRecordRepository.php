<?php

namespace App\Repository;

use App\Entity\KnowledgeBaseRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KnowledgeBaseRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method KnowledgeBaseRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method KnowledgeBaseRecord[]    findAll()
 * @method KnowledgeBaseRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KnowledgeBaseRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KnowledgeBaseRecord::class);
    }

    // /**
    //  * @return KnowledgeBaseRecord[] Returns an array of KnowledgeBaseRecord objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KnowledgeBaseRecord
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @return KnowledgeBaseRecord[] Returns an array of KnowledgeBaseRecord objects
     */
    public function findOrderedByUpdateDate()
    {
        return $this->createQueryBuilder('k')
            ->orderBy('k.updatedAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
