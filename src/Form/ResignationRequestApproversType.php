<?php

namespace App\Form;

use App\Entity\ResignationRequestApprovers;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ResignationRequestApproversType extends AbstractType
{
    // private $role = array();
    // public function __construct(array $role) {
    //      $this->role = $role;
    // }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $role = $options['role'];
        $builder
            //->add('role')
            //->add('insertedAt')
            ->add('approver', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($role) {
                    return $er->createQueryBuilder('u')->where("u.roles LIKE '%" . $role. "%'");
                },
                'choice_label' => function ($approver) {
                    return $approver->getName() . " " . $approver->getLastname();
                },
            ])
            //->add('insertor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResignationRequestApprovers::class,
        ]);
        $resolver->setRequired([
            'role'
        ]);
    }
}
