<?php

namespace App\Form;

use App\Entity\KnowledgeBaseRecord;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class KnowledgeBaseRecordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content', CKEditorType::class,  array(
                'config' => array('toolbar' => 'standard'),
            )
            )
            // ->add('createdAt')
            // ->add('updatedAt')
            // ->add('author')
            // ->add('lastContributor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KnowledgeBaseRecord::class,
        ]);
    }
}
