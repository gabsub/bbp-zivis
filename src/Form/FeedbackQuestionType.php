<?php

namespace App\Form;

use App\Entity\FeedbackQuestion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FeedbackQuestionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('number')
            ->add('answerType', ChoiceType::class, [
                'choices'=> [
                    'Tekstinis laukas' => 'string',
                    'Taip/Ne' => 'bool',
                    'Įvertinimas balais' => 'integer',
                ],
            ])
            ->add('question')
            //->add('createdAt')
            //->add('updatedAt')
            ->add('activeStatus')
            //->add('author')
            //->add('lastContributor')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FeedbackQuestion::class,
        ]);
    }
}
