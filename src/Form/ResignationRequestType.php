<?php

namespace App\Form;

use App\Entity\ResignationRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ResignationRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('submittedAt')
            ->add('quittingAt', TextType::class)
            //->add('status')
            //->add('approvedByManagerAt')
            //->add('approvedByAdministratorAt')
            //->add('approvedByAccountantAt')
            //->add('approvedByITadminAt')
            //->add('approvedByLawyerAt')
            //->add('statusByManager')
            //->add('statusByAdministrator')
            //->add('statusByAccountant')
            //->add('statusByITadmin')
            //->add('statusByLawyer')
            //->add('submitter')
            //->add('approverManager')
            //->add('approverAdministrator')
            //->add('approverAccountant')
            //->add('approverITadmin')
            //->add('approverLawyer')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResignationRequest::class,
        ]);
    }
}
