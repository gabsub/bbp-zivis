<?php

namespace App\Form;

use App\Entity\VacationRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\VacationType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Security;

class VacationRequestType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startsAt', TextType::class)
            ->add('finishesAt', TextType::class)
            //->add('createdAt')
            //->add('acceptedAt')
            //->add('status')
            //->add('submitter')
            ->add('substitute', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->findByManagerId($this->security->getUser()->getManager()->getId(), $this->security->getUser()->getId());
                },
                'choice_label' => function ($substitute) {
                    return $substitute->getName() . " " . $substitute->getLastname();
                },
            ])
            //->add('approver')
            ->add('vacationType', EntityType::class, [
                'class' => VacationType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u');
                },
                'choice_label' => 'title'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VacationRequest::class,
        ]);
    }
}
