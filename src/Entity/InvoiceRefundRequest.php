<?php

namespace App\Entity;

use App\Repository\InvoiceRefundRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRefundRequestRepository::class)
 */
class InvoiceRefundRequest
{
    public const STATUS_SUBMITTED = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_SUBMITTED_TEXT = 'Pateikta';
    public const STATUS_APPROVED_TEXT = 'Patvirtinta';
    public const STATUS_REJECTED_TEXT = 'Atmesta';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="invoiceRefundRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $submitter;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="invoiceRefundRequests")
     */
    private $approver;

    /**
     * @ORM\Column(type="float")
     */
    private $refundAmount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $invoiceFileName;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $acceptedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSubmitter(): ?User
    {
        return $this->submitter;
    }

    public function setSubmitter(?User $submitter): self
    {
        $this->submitter = $submitter;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getApprover(): ?User
    {
        return $this->approver;
    }

    public function setApprover(?User $approver): self
    {
        $this->approver = $approver;

        return $this;
    }

    public function getRefundAmount(): ?float
    {
        return $this->refundAmount;
    }

    public function setRefundAmount(float $refundAmount): self
    {
        $this->refundAmount = $refundAmount;

        return $this;
    }

    public function getInvoiceFileName(): ?string
    {
        return $this->invoiceFileName;
    }

    public function setInvoiceFileName(string $invoiceFileName): self
    {
        $this->invoiceFileName = $invoiceFileName;

        return $this;
    }

    public function getAcceptedAt(): ?\DateTimeInterface
    {
        return $this->acceptedAt;
    }

    public function setAcceptedAt(?\DateTimeInterface $acceptedAt): self
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }
}
