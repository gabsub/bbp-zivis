<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = ["ROLE_USER"];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="employees")
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="manager")
     */
    private $employees;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $salary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfEmployment;

    /**
     * @ORM\OneToMany(targetEntity=KnowledgeBaseRecord::class, mappedBy="author")
     */
    private $knowledgeBaseRecords;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentType::class, inversedBy="users")
     */
    private $paymentType;

    /**
     * @ORM\OneToMany(targetEntity=VacationRequest::class, mappedBy="submitter")
     */
    private $vacationRequests;

    /**
     * @ORM\OneToMany(targetEntity=InvoiceRefundRequest::class, mappedBy="submitter")
     */
    private $invoiceRefundRequests;

    /**
     * @ORM\OneToMany(targetEntity=ResignationRequestApprovers::class, mappedBy="approver")
     */
    private $resignationRequestApprovers;

    /**
     * @ORM\OneToMany(targetEntity=ResignationRequest::class, mappedBy="submitter")
     */
    private $resignationRequests;

    /**
     * @ORM\OneToMany(targetEntity=FeedbackQuestion::class, mappedBy="author")
     */
    private $feedbackQuestions;

    /**
     * @ORM\OneToMany(targetEntity=Feedback::class, mappedBy="submitter")
     */
    private $feedback;

    public function __construct()
    {
        $this->employees = new ArrayCollection();
        $this->knowledgeBaseRecords = new ArrayCollection();
        $this->vacationRequests = new ArrayCollection();
        $this->invoiceRefundRequests = new ArrayCollection();
        $this->resignationRequestApprovers = new ArrayCollection();
        $this->resignationRequests = new ArrayCollection();
        $this->feedbackQuestions = new ArrayCollection();
        $this->feedback = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getManager(): ?self
    {
        return $this->manager;
    }

    public function setManager(?self $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(self $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->setManager($this);
        }

        return $this;
    }

    public function removeEmployee(self $employee): self
    {
        if ($this->employees->removeElement($employee)) {
            // set the owning side to null (unless already changed)
            if ($employee->getManager() === $this) {
                $employee->setManager(null);
            }
        }

        return $this;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function setSalary(?float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getDateOfEmployment(): ?\DateTimeInterface
    {
        return $this->dateOfEmployment;
    }

    public function setDateOfEmployment(\DateTimeInterface $dateOfEmployment): self
    {
        $this->dateOfEmployment = $dateOfEmployment;

        return $this;
    }

    /**
     * @return Collection|KnowledgeBaseRecord[]
     */
    public function getKnowledgeBaseRecords(): Collection
    {
        return $this->knowledgeBaseRecords;
    }

    public function addKnowledgeBaseRecord(KnowledgeBaseRecord $knowledgeBaseRecord): self
    {
        if (!$this->knowledgeBaseRecords->contains($knowledgeBaseRecord)) {
            $this->knowledgeBaseRecords[] = $knowledgeBaseRecord;
            $knowledgeBaseRecord->setAuthor($this);
        }

        return $this;
    }

    public function removeKnowledgeBaseRecord(KnowledgeBaseRecord $knowledgeBaseRecord): self
    {
        if ($this->knowledgeBaseRecords->removeElement($knowledgeBaseRecord)) {
            // set the owning side to null (unless already changed)
            if ($knowledgeBaseRecord->getAuthor() === $this) {
                $knowledgeBaseRecord->setAuthor(null);
            }
        }

        return $this;
    }

    public function getPaymentType(): ?PaymentType
    {
        return $this->paymentType;
    }

    public function setPaymentType(?PaymentType $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * @return Collection|VacationRequest[]
     */
    public function getVacationRequests(): Collection
    {
        return $this->vacationRequests;
    }

    public function addVacationRequest(VacationRequest $vacationRequest): self
    {
        if (!$this->vacationRequests->contains($vacationRequest)) {
            $this->vacationRequests[] = $vacationRequest;
            $vacationRequest->setSubmitter($this);
        }

        return $this;
    }

    public function removeVacationRequest(VacationRequest $vacationRequest): self
    {
        if ($this->vacationRequests->removeElement($vacationRequest)) {
            // set the owning side to null (unless already changed)
            if ($vacationRequest->getSubmitter() === $this) {
                $vacationRequest->setSubmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InvoiceRefundRequest[]
     */
    public function getInvoiceRefundRequests(): Collection
    {
        return $this->invoiceRefundRequests;
    }

    public function addInvoiceRefundRequest(InvoiceRefundRequest $invoiceRefundRequest): self
    {
        if (!$this->invoiceRefundRequests->contains($invoiceRefundRequest)) {
            $this->invoiceRefundRequests[] = $invoiceRefundRequest;
            $invoiceRefundRequest->setSubmitter($this);
        }

        return $this;
    }

    public function removeInvoiceRefundRequest(InvoiceRefundRequest $invoiceRefundRequest): self
    {
        if ($this->invoiceRefundRequests->removeElement($invoiceRefundRequest)) {
            // set the owning side to null (unless already changed)
            if ($invoiceRefundRequest->getSubmitter() === $this) {
                $invoiceRefundRequest->setSubmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResignationRequestApprovers[]
     */
    public function getResignationRequestApprovers(): Collection
    {
        return $this->resignationRequestApprovers;
    }

    public function addResignationRequestApprover(ResignationRequestApprovers $resignationRequestApprover): self
    {
        if (!$this->resignationRequestApprovers->contains($resignationRequestApprover)) {
            $this->resignationRequestApprovers[] = $resignationRequestApprover;
            $resignationRequestApprover->setApprover($this);
        }

        return $this;
    }

    public function removeResignationRequestApprover(ResignationRequestApprovers $resignationRequestApprover): self
    {
        if ($this->resignationRequestApprovers->removeElement($resignationRequestApprover)) {
            // set the owning side to null (unless already changed)
            if ($resignationRequestApprover->getApprover() === $this) {
                $resignationRequestApprover->setApprover(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResignationRequest[]
     */
    public function getResignationRequests(): Collection
    {
        return $this->resignationRequests;
    }

    public function addResignationRequest(ResignationRequest $resignationRequest): self
    {
        if (!$this->resignationRequests->contains($resignationRequest)) {
            $this->resignationRequests[] = $resignationRequest;
            $resignationRequest->setSubmitter($this);
        }

        return $this;
    }

    public function removeResignationRequest(ResignationRequest $resignationRequest): self
    {
        if ($this->resignationRequests->removeElement($resignationRequest)) {
            // set the owning side to null (unless already changed)
            if ($resignationRequest->getSubmitter() === $this) {
                $resignationRequest->setSubmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FeedbackQuestion[]
     */
    public function getFeedbackQuestions(): Collection
    {
        return $this->feedbackQuestions;
    }

    public function addFeedbackQuestion(FeedbackQuestion $feedbackQuestion): self
    {
        if (!$this->feedbackQuestions->contains($feedbackQuestion)) {
            $this->feedbackQuestions[] = $feedbackQuestion;
            $feedbackQuestion->setAuthor($this);
        }

        return $this;
    }

    public function removeFeedbackQuestion(FeedbackQuestion $feedbackQuestion): self
    {
        if ($this->feedbackQuestions->removeElement($feedbackQuestion)) {
            // set the owning side to null (unless already changed)
            if ($feedbackQuestion->getAuthor() === $this) {
                $feedbackQuestion->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Feedback[]
     */
    public function getFeedback(): Collection
    {
        return $this->feedback;
    }

    public function addFeedback(Feedback $feedback): self
    {
        if (!$this->feedback->contains($feedback)) {
            $this->feedback[] = $feedback;
            $feedback->setSubmitter($this);
        }

        return $this;
    }

    public function removeFeedback(Feedback $feedback): self
    {
        if ($this->feedback->removeElement($feedback)) {
            // set the owning side to null (unless already changed)
            if ($feedback->getSubmitter() === $this) {
                $feedback->setSubmitter(null);
            }
        }

        return $this;
    }
}
