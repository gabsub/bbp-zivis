<?php

namespace App\Entity;

use App\Repository\VacationRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=VacationRequestRepository::class)
 */
class VacationRequest
{
    public const STATUS_SUBMITTED = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_SUBMITTED_TEXT = 'Pateikta';
    public const STATUS_APPROVED_TEXT = 'Patvirtinta';
    public const STATUS_REJECTED_TEXT = 'Atmesta';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="vacationRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $submitter;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startsAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $finishesAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $acceptedAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $substitute;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approver;

    /**
     * @ORM\ManyToOne(targetEntity=VacationType::class, inversedBy="vacationRequests")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Nepasirinktas atostogų tipas")
     */
    private $vacationType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubmitter(): ?User
    {
        return $this->submitter;
    }

    public function setSubmitter(?User $submitter): self
    {
        $this->submitter = $submitter;

        return $this;
    }

    public function getStartsAt(): ?\DateTimeInterface
    {
        return $this->startsAt;
    }

    public function setStartsAt(string $startsAt): self
    {
        $this->startsAt = new \DateTime($startsAt);

        return $this;
    }

    public function getFinishesAt(): ?\DateTimeInterface
    {
        return $this->finishesAt;
    }

    public function setFinishesAt(string $finishesAt): self
    {
        $this->finishesAt = new \DateTime($finishesAt);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAcceptedAt(): ?\DateTimeInterface
    {
        return $this->acceptedAt;
    }

    public function setAcceptedAt(?\DateTimeInterface $acceptedAt): self
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSubstitute(): ?User
    {
        return $this->substitute;
    }

    public function setSubstitute(?User $substitute): self
    {
        $this->substitute = $substitute;

        return $this;
    }

    public function getApprover(): ?User
    {
        return $this->approver;
    }

    public function setApprover(?User $approver): self
    {
        $this->approver = $approver;

        return $this;
    }

    public function getVacationType(): ?VacationType
    {
        return $this->vacationType;
    }

    public function setVacationType(?VacationType $vacationType): self
    {
        $this->vacationType = $vacationType;

        return $this;
    }
}
