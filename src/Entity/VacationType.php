<?php

namespace App\Entity;

use App\Repository\VacationTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VacationTypeRepository::class)
 */
class VacationType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=VacationRequest::class, mappedBy="vacationType")
     */
    private $vacationRequests;

    public function __construct()
    {
        $this->vacationRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|VacationRequest[]
     */
    public function getVacationRequests(): Collection
    {
        return $this->vacationRequests;
    }

    public function addVacationRequest(VacationRequest $vacationRequest): self
    {
        if (!$this->vacationRequests->contains($vacationRequest)) {
            $this->vacationRequests[] = $vacationRequest;
            $vacationRequest->setVacationType($this);
        }

        return $this;
    }

    public function removeVacationRequest(VacationRequest $vacationRequest): self
    {
        if ($this->vacationRequests->removeElement($vacationRequest)) {
            // set the owning side to null (unless already changed)
            if ($vacationRequest->getVacationType() === $this) {
                $vacationRequest->setVacationType(null);
            }
        }

        return $this;
    }
}
