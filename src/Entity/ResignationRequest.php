<?php

namespace App\Entity;

use App\Repository\ResignationRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResignationRequestRepository::class)
 */
class ResignationRequest
{
    public const STATUS_SUBMITTED = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_SUBMITTED_TEXT = 'Pateikta';
    public const STATUS_APPROVED_TEXT = 'Patvirtinta';
    public const STATUS_REJECTED_TEXT = 'Atmesta';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="resignationRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $submitter;

    /**
     * @ORM\Column(type="datetime")
     */
    private $submittedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $quittingAt;

    /**
     * @ORM\Column(type="integer")
     */
    private $status = 0;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approverManager;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approvedByManagerAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approverAdministrator;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approvedByAdministratorAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approverAccountant;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approvedByAccountantAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approverITadmin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approvedByITadminAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $approverLawyer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $approvedByLawyerAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusByManager = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusByAdministrator = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusByAccountant = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusByITadmin = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $statusByLawyer = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubmitter(): ?User
    {
        return $this->submitter;
    }

    public function setSubmitter(?User $submitter): self
    {
        $this->submitter = $submitter;

        return $this;
    }

    public function getSubmittedAt(): ?\DateTimeInterface
    {
        return $this->submittedAt;
    }

    public function setSubmittedAt(\DateTimeInterface $submittedAt): self
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    public function getQuittingAt(): ?\DateTimeInterface
    {
        return $this->quittingAt;
    }

    public function setQuittingAt(string $quittingAt): self
    {
        $this->quittingAt = new \DateTime($quittingAt);

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getApproverManager(): ?User
    {
        return $this->approverManager;
    }

    public function setApproverManager(?User $approverManager): self
    {
        $this->approverManager = $approverManager;

        return $this;
    }

    public function getApprovedByManagerAt(): ?\DateTimeInterface
    {
        return $this->approvedByManagerAt;
    }

    public function setApprovedByManagerAt(?\DateTimeInterface $approvedByManagerAt): self
    {
        $this->approvedByManagerAt = $approvedByManagerAt;

        return $this;
    }

    public function getApproverAdministrator(): ?User
    {
        return $this->approverAdministrator;
    }

    public function setApproverAdministrator(?User $approverAdministrator): self
    {
        $this->approverAdministrator = $approverAdministrator;

        return $this;
    }

    public function getApprovedByAdministratorAt(): ?\DateTimeInterface
    {
        return $this->approvedByAdministratorAt;
    }

    public function setApprovedByAdministratorAt(?\DateTimeInterface $approvedByAdministratorAt): self
    {
        $this->approvedByAdministratorAt = $approvedByAdministratorAt;

        return $this;
    }

    public function getApproverAccountant(): ?User
    {
        return $this->approverAccountant;
    }

    public function setApproverAccountant(?User $approverAccountant): self
    {
        $this->approverAccountant = $approverAccountant;

        return $this;
    }

    public function getApprovedByAccountantAt(): ?\DateTimeInterface
    {
        return $this->approvedByAccountantAt;
    }

    public function setApprovedByAccountantAt(?\DateTimeInterface $approvedByAccountantAt): self
    {
        $this->approvedByAccountantAt = $approvedByAccountantAt;

        return $this;
    }

    public function getApproverITadmin(): ?User
    {
        return $this->approverITadmin;
    }

    public function setApproverITadmin(?User $approverITadmin): self
    {
        $this->approverITadmin = $approverITadmin;

        return $this;
    }

    public function getApprovedByITadminAt(): ?\DateTimeInterface
    {
        return $this->approvedByITadminAt;
    }

    public function setApprovedByITadminAt(?\DateTimeInterface $approvedByITadminAt): self
    {
        $this->approvedByITadminAt = $approvedByITadminAt;

        return $this;
    }

    public function getApproverLawyer(): ?User
    {
        return $this->approverLawyer;
    }

    public function setApproverLawyer(?User $approverLawyer): self
    {
        $this->approverLawyer = $approverLawyer;

        return $this;
    }

    public function getApprovedByLawyerAt(): ?\DateTimeInterface
    {
        return $this->approvedByLawyerAt;
    }

    public function setApprovedByLawyerAt(?\DateTimeInterface $approvedByLawyerAt): self
    {
        $this->approvedByLawyerAt = $approvedByLawyerAt;

        return $this;
    }

    public function getStatusByManager(): ?int
    {
        return $this->statusByManager;
    }

    public function setStatusByManager(?int $statusByManager): self
    {
        $this->statusByManager = $statusByManager;

        return $this;
    }

    public function getStatusByAdministrator(): ?int
    {
        return $this->statusByAdministrator;
    }

    public function setStatusByAdministrator(?int $statusByAdministrator): self
    {
        $this->statusByAdministrator = $statusByAdministrator;

        return $this;
    }

    public function getStatusByAccountant(): ?int
    {
        return $this->statusByAccountant;
    }

    public function setStatusByAccountant(?int $statusByAccountant): self
    {
        $this->statusByAccountant = $statusByAccountant;

        return $this;
    }

    public function getStatusByITadmin(): ?int
    {
        return $this->statusByITadmin;
    }

    public function setStatusByITadmin(?int $statusByITadmin): self
    {
        $this->statusByITadmin = $statusByITadmin;

        return $this;
    }

    public function getStatusByLawyer(): ?int
    {
        return $this->statusByLawyer;
    }

    public function setStatusByLawyer(?int $statusByLawyer): self
    {
        $this->statusByLawyer = $statusByLawyer;

        return $this;
    }
}
