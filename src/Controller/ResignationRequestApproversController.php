<?php

namespace App\Controller;

use App\Entity\ResignationRequestApprovers;
use App\Form\ResignationRequestApproversType;
use App\Repository\ResignationRequestApproversRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\User;
use App\Repository\UserRepository;

/**
 * @Security("is_granted('ROLE_ADMIN')")
 * @Route("/resignation-request-approvers")
 */
class ResignationRequestApproversController extends AbstractController
{   
    /**
     * @Route("/", name="resignation_request_approvers_index", methods={"GET"})
     */
    public function index(ResignationRequestApproversRepository $resignationRequestApproversRepository): Response
    {
        return $this->render('resignation_request_approvers/index.html.twig', [
            'resignation_request_approvers' => $resignationRequestApproversRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="resignation_request_approvers_show", methods={"GET"})
     */
    public function show(ResignationRequestApprovers $resignationRequestApprover): Response
    {
        return $this->render('resignation_request_approvers/show.html.twig', [
            'resignation_request_approver' => $resignationRequestApprover,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="resignation_request_approvers_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ResignationRequestApprovers $resignationRequestApprover, ResignationRequestApproversRepository $resignationRequestApproversRepository, UserRepository $userRepository): Response
    {
        $approverId = $resignationRequestApproversRepository->findOneBy(['id' =>  $request->get('id') ])->getApprover()->getId();
        $roleApprover = $resignationRequestApproversRepository->findOneBy(['id' =>  $request->get('id') ])->getRole();
        if($roleApprover == 'Administatorius'){
            $role = 'ROLE_MODERATOR';
        }
        elseif ($roleApprover == 'Buhalteris') {
            $role = 'ROLE_ACCOUNTANT';
        }
        elseif ($roleApprover == 'IT administratorius') {
            $role = 'ROLE_ADMIN';
        }
        elseif ($roleApprover == 'Teisininkas') {
            $role = 'ROLE_LAWYER';
        } else {
            $role = 'ROLE_USER';
        }
        $form = $this->createForm(ResignationRequestApproversType::class, $resignationRequestApprover, ['role' => $role] );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resignationRequestApprover->setInsertor($this->getUser());
            $resignationRequestApprover->setInsertedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            $entityManager = $this->getDoctrine()->getManager();
            $deselectedApprover = $userRepository->findOneBy(['id' => $approverId]);
            $roles = $deselectedApprover->getRoles();
            $key = array_search('ROLE_APPROVER', $roles);
            if ($key !== false) {
                unset($roles[$key]);
            }
            $deselectedApprover->setRoles($roles);
            $entityManager->persist($deselectedApprover);
            $entityManager->flush();
            $entityManager = $this->getDoctrine()->getManager();
            $selectedApprover = $userRepository->findOneBy(['id' => $resignationRequestApprover->getApprover()->getId()]);
            $roles = $selectedApprover->getRoles();
            array_push($roles, 'ROLE_APPROVER');
            $selectedApprover->setRoles($roles);
            $entityManager->persist($selectedApprover);
            $entityManager->flush();
            return $this->redirectToRoute('resignation_request_approvers_index');
        }

        return $this->render('resignation_request_approvers/edit.html.twig', [
            'resignation_request_approver' => $resignationRequestApprover,
            'form' => $form->createView(),
        ]);
    }
}
