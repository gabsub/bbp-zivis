<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_ACCOUNTANT')")
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/hierarchy", name="user_hierarchy", methods={"GET"})
     */
    public function hierarchy(UserRepository $userRepository) : Response
    {
        $managers = $userRepository->findBy(['manager' => null]);
        $tree = array();
        foreach($managers as $manager){
            $tree[$manager->getId()] = null;
            if(!empty($this->lookForChildren($userRepository, $manager))){
                $tree[$manager->getId()] = $this->lookForChildren($userRepository, $manager);
            }
        }
        return $this->render('user/hierarchy.html.twig', [
            'users' => $userRepository->findAll(),
            'tree' => $tree
        ]);
    }

    private function lookForChildren(UserRepository $userRepository, User $manager)
    {
        $children = $manager->getEmployees();
        $tree = array();
        foreach($children as $child){
            $tree[$child->getId()] = null;
            if(!empty($this->lookForChildren($userRepository, $child))){
                $tree[$child->getId()] = $this->lookForChildren($userRepository, $child);
            }
        }
        return $tree;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/role", name="user_role", methods={"GET"})
     */
    public function roles(UserRepository $userRepository): Response
    {
        return $this->render('user/roles.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ACCOUNTANT')")
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/edit-roles", name="user_edit_roles", methods={"GET"})
     */
    public function editRoles(User $user): Response
    {
        return $this->render('user/editroles.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/manager", name="user_add_role_manager")
     */
    public function addRoleManager(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_MANAGER');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/manager", name="user_remove_role_manager")
     */
    public function removeRoleManager(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_MANAGER', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/accountant", name="user_add_role_accountant")
     */
    public function addRoleAccountant(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_ACCOUNTANT');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/accountant", name="user_remove_role_accountant")
     */
    public function removeRoleAccountant(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_ACCOUNTANT', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/lawyer", name="user_add_role_lawyer")
     */
    public function addRoleLawyer(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_LAWYER');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/lawyer", name="user_remove_role_lawyer")
     */
    public function removeRoleLawyer(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_LAWYER', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/moderator", name="user_add_role_moderator")
     */
    public function addRoleModerator(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_MODERATOR');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/moderator", name="user_remove_role_moderator")
     */
    public function removeRoleModerator(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_MODERATOR', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/approver", name="user_add_role_approver")
     */
    public function addRoleApprover(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_APPROVER');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/approver", name="user_remove_role_approver")
     */
    public function removeRoleApprover(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_APPROVER', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/add-role/admin", name="user_add_role_admin")
     */
    public function addRoleAdmin(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        array_push($roles, 'ROLE_ADMIN');
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Route("/{id}/remove-role/admin", name="user_remove_role_admin")
     */
    public function removeRoleAdmin(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $roles = $user->getRoles();
        $key = array_search('ROLE_ADMIN', $roles);
        if ($key !== false) {
            unset($roles[$key]);
        }
        $user->setRoles($roles);
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->redirectToRoute('user_edit_roles', [ 'id' => $user->getId() ] );
    }
}
