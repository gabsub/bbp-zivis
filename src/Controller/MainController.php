<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcher;

class MainController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_USER')")
     * @Route("/", name="main")
     */
    public function index(UserRepository $user_rep, Request $request): Response
    {   
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'user_id' => $this->getUser()->getId(),
        ]);
    }
}
