<?php

namespace App\Controller;

use App\Entity\KnowledgeBaseRecord;
use App\Form\KnowledgeBaseRecordType;
use App\Repository\KnowledgeBaseRecordRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/** 
 * @Security("is_granted('ROLE_USER')")
 * @Route("/knowledge-base")
 */
class KnowledgeBaseRecordController extends AbstractController
{
    /**
     * @Route("/", name="knowledge_base_record_index", methods={"GET"})
     */
    public function index(KnowledgeBaseRecordRepository $knowledgeBaseRecordRepository): Response
    {
        return $this->render('knowledge_base_record/index.html.twig', [
            'knowledge_base_records' => $knowledgeBaseRecordRepository->findOrderedByUpdateDate(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_MODERATOR')")
     * @Route("/new", name="knowledge_base_record_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $knowledgeBaseRecord = new KnowledgeBaseRecord();
        $form = $this->createForm(KnowledgeBaseRecordType::class, $knowledgeBaseRecord);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $knowledgeBaseRecord->setCreatedAt(new \DateTime());
            $knowledgeBaseRecord->setUpdatedAt(new \DateTime());
            $knowledgeBaseRecord->setAuthor($this->getUser());
            $knowledgeBaseRecord->setLastContributor($this->getUser());

            if($knowledgeBaseRecord->getContent() == null){
                $this->addFlash(
                    'content',
                    'Žinių bazės įrašo turinio laukas negali būti tuščias'
                );
                return $this->redirectToRoute('knowledge_base_record_new');
            } else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($knowledgeBaseRecord);
                $entityManager->flush();

                return $this->redirectToRoute('knowledge_base_record_index');
            }
        }

        return $this->render('knowledge_base_record/new.html.twig', [
            'knowledge_base_record' => $knowledgeBaseRecord,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="knowledge_base_record_show", methods={"GET"})
     */
    public function show(KnowledgeBaseRecord $knowledgeBaseRecord): Response
    {
        return $this->render('knowledge_base_record/show.html.twig', [
            'knowledge_base_record' => $knowledgeBaseRecord,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_MODERATOR')")
     * @Route("/{id}/edit", name="knowledge_base_record_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, KnowledgeBaseRecord $knowledgeBaseRecord): Response
    {
        $form = $this->createForm(KnowledgeBaseRecordType::class, $knowledgeBaseRecord);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($knowledgeBaseRecord->getContent() == null){
                $this->addFlash(
                    'content',
                    'Žinių bazės įrašo turinio laukas negali būti tuščias'
                );
                return $this->redirectToRoute('knowledge_base_record_edit', ['id' => $request->get('id')]);
            }
            $knowledgeBaseRecord->setUpdatedAt(new \DateTime());
            $knowledgeBaseRecord->setLastContributor($this->getUser());
            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('knowledge_base_record_index');
            //return $this->redirectToRoute('send_email');
        }

        return $this->render('knowledge_base_record/edit.html.twig', [
            'knowledge_base_record' => $knowledgeBaseRecord,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_MODERATOR')")
     * @Route("/{id}", name="knowledge_base_record_delete", methods={"POST"})
     */
    public function delete(Request $request, KnowledgeBaseRecord $knowledgeBaseRecord): Response
    {
        if ($this->isCsrfTokenValid('delete'.$knowledgeBaseRecord->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($knowledgeBaseRecord);
            $entityManager->flush();
        }

        return $this->redirectToRoute('knowledge_base_record_index');
    }
}
