<?php

namespace App\Controller;

use App\Entity\FeedbackQuestion;
use App\Form\FeedbackQuestionType;
use App\Repository\FeedbackQuestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_MODERATOR')")
 * @Route("/feedback-question")
 */
class FeedbackQuestionController extends AbstractController
{
    /**
     * @Route("/", name="feedback_question_index", methods={"GET"})
     */
    public function index(FeedbackQuestionRepository $feedbackQuestionRepository): Response
    {
        return $this->render('feedback_question/index.html.twig', [
            'feedback_questions' => [ 'active' => $feedbackQuestionRepository->findActiveQuestions(),
                                    'notactive' => $feedbackQuestionRepository->findNotActiveQuestions(),
                                    'maxQuestionNumber' => $feedbackQuestionRepository->findMaxNumber() ],
        ]);
    }


    /**
     * @Route("/new", name="feedback_question_new", methods={"GET","POST"})
     */
    public function new(Request $request, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $feedbackQuestion = new FeedbackQuestion();
        $form = $this->createForm(FeedbackQuestionType::class, $feedbackQuestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedbackQuestion->setAuthor($this->getUser());
            $feedbackQuestion->setLastContributor($this->getUser());
            $feedbackQuestion->setCreatedAt(new \DateTime());
            $feedbackQuestion->setUpdatedAt(new \DateTime());
            if($feedbackQuestion->getActiveStatus()){
                $number = $feedbackQuestionRepository->findMaxNumber()->getNumber();
                $feedbackQuestion->setNumber($number + 1);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedbackQuestion);
            $entityManager->flush();

            return $this->redirectToRoute('feedback_question_index');
        }

        return $this->render('feedback_question/new.html.twig', [
            'feedback_question' => $feedbackQuestion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="feedback_question_show", methods={"GET"})
     */
    public function show(FeedbackQuestion $feedbackQuestion): Response
    {
        return $this->render('feedback_question/show.html.twig', [
            'feedback_question' => $feedbackQuestion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="feedback_question_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FeedbackQuestion $feedbackQuestion, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $form = $this->createForm(FeedbackQuestionType::class, $feedbackQuestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $feedbackQuestion->setLastContributor($this->getUser());
            $feedbackQuestion->setUpdatedAt(new \DateTime());
            if($feedbackQuestion->getActiveStatus() && $feedbackQuestion->getNumber() == 0){
                $number = $feedbackQuestionRepository->findMaxNumber()->getNumber();
                $feedbackQuestion->setNumber($number + 1);
            }
            if(!$feedbackQuestion->getActiveStatus()){
                $number = $feedbackQuestion->getNumber();
                $max = $feedbackQuestionRepository->findMaxNumber()->getNumber();
                for( $i = $number+1; $i <= $max; $i++ ){
                    $object = $feedbackQuestionRepository->findOneByNumber($i);
                    $object->setNumber($i-1);
                    $this->getDoctrine()->getManager()->flush();
                }
                $feedbackQuestion->setNumber(0);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('feedback_question_index');
        }

        return $this->render('feedback_question/edit.html.twig', [
            'feedback_question' => $feedbackQuestion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/up", name="feedback_question_up")
     */
    public function up(Request $request, FeedbackQuestion $feedbackQuestion, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $number = $feedbackQuestion->getNumber();
        $goesDown = $feedbackQuestionRepository->findOneByNumber($number-1);
        $feedbackQuestion->setNumber($number-1);
        $goesDown->setNumber($number);
        $entityManager->persist($feedbackQuestion);
        $entityManager->flush();

        return $this->redirectToRoute('feedback_question_index');
    }

    /**
     * @Route("/{id}/down", name="feedback_question_down")
     */
    public function down(Request $request, FeedbackQuestion $feedbackQuestion, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $number = $feedbackQuestion->getNumber();
        $goesDown = $feedbackQuestionRepository->findOneByNumber($number+1);
        $feedbackQuestion->setNumber($number+1);
        $goesDown->setNumber($number);
        $entityManager->persist($feedbackQuestion);
        $entityManager->flush();

        return $this->redirectToRoute('feedback_question_index');
    }

    /**
     * @Route("/{id}/activate", name="feedback_question_activate")
     */
    public function activate(Request $request, FeedbackQuestion $feedbackQuestion, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $number = $feedbackQuestionRepository->findMaxNumber()->getNumber();
        $feedbackQuestion->setNumber($number + 1);
        $feedbackQuestion->setActiveStatus(true);
        $entityManager->persist($feedbackQuestion);
        $entityManager->flush();

        return $this->redirectToRoute('feedback_question_index');
    }

    /**
     * @Route("/{id}/deactivate", name="feedback_question_deactivate")
     */
    public function deactivate(Request $request, FeedbackQuestion $feedbackQuestion, FeedbackQuestionRepository $feedbackQuestionRepository ): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $number = $feedbackQuestion->getNumber();
        $max = $feedbackQuestionRepository->findMaxNumber()->getNumber();
        for( $i = $number+1; $i <= $max; $i++ ){
            $object = $feedbackQuestionRepository->findOneByNumber($i);
            $object->setNumber($i-1);
            $entityManager->flush();
        }
        $feedbackQuestion->setNumber(0);
        $feedbackQuestion->setActiveStatus(false);
        $entityManager->flush();

        return $this->redirectToRoute('feedback_question_index');
    }
}
