<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/email")
 */
class MailerController extends AbstractController
{
    /**
     * @Route("/email/invoice-refund-request", name="send_email_invoice_refund")
     */
    public function sendEmailInvoiceRefundRequest(MailerInterface $mailer, UserRepository $userRepository)
    {
        $accountants = $userRepository->findAll();
        foreach($accountants as $accountant){
            if(in_array('ROLE_ACCOUNTANT', $accountant->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($accountant->getEmail())
                ->subject('Įkeltas naujas prašymas apmokėti sąskaitą faktūrą')
                ->html('<p>Įkeltas naujas prašymas apmokėti sąskaitą faktūrą.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('request_index');
    }

    /**
     * @Route("/email/invoice-refund-request/approved/{id}", name="send_email_invoice_refund_approved")
     */
    public function sendEmailInvoiceRefundRequestApproved(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų prašymas apmokėti sąskaitą faktūrą patvirtintas')
            ->html('<p>Jūsų prašymas apmokėti sąskaitą faktūrą patvirtintas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/invoice-refund-request/rejected/{id}", name="send_email_invoice_refund_rejected")
     */
    public function sendEmailInvoiceRefundRequestRejected(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų prašymas apmokėti sąskaitą faktūrą atmestas')
            ->html('<p>Jūsų prašymas apmokėti sąskaitą faktūrą atmestas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/vacation-request", name="send_email_vacation")
     */
    public function sendEmailVacationRequest(MailerInterface $mailer, UserRepository $userRepository)
    {
        $managers = $userRepository->findBy(['id' => $this->getUser()->getManager()->getId()]);
        foreach($managers as $manager){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($manager->getEmail())
            ->subject('Įkeltas naujas atostogų prašymas')
            ->html('<p>Įkeltas naujas atostogų prašymas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_index');
    }

    /**
     * @Route("/email/vacation-request/approved/{id}", name="send_email_resignation_approved")
     */
    public function sendEmailVacationRequestApproved(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų atostogų prašymas patvirtintas')
            ->html('<p>Jūsų atostogų prašymas patvirtintas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/vacation-request/rejected/{id}", name="send_email_resignation_rejected")
     */
    public function sendEmailVacationRequestRejected(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų atostogų prašymas atmestas')
            ->html('<p>Jūsų atostogų prašymas atmestas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/IT", name="send_email_resignation_it")
     */
    public function sendEmailResignationRequestIT(MailerInterface $mailer, UserRepository $userRepository)
    {
        $itadmins = $userRepository->findAll();
        foreach($itadmins as $itadmin){
            if(in_array('ROLE_APPROVER', $itadmin->getRoles()) and in_array('ROLE_ADMIN', $itadmin->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($itadmin->getEmail())
                ->subject('Įkeltas naujas išėjimo iš darbo prašymas')
                ->html('<p>Įkeltas naujas išėjimo iš darbo prašymas.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('request_index');
    }

    /**
     * @Route("/email/resigantion-request/accountant", name="send_email_resignation_accountant")
     */
    public function sendEmailResignationRequestAccountant(MailerInterface $mailer, UserRepository $userRepository)
    {
        $accountants = $userRepository->findAll();
        foreach($accountants as $accountant){
            if(in_array('ROLE_APPROVER', $accountant->getRoles()) and in_array('ROLE_ACCOUNTANT', $accountant->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($accountant->getEmail())
                ->subject('Įkeltas naujas išėjimo iš darbo prašymas')
                ->html('<p>Įkeltas naujas išėjimo iš darbo prašymas.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/lawyer", name="send_email_resignation_lawyer")
     */
    public function sendEmailResignationRequestLawyer(MailerInterface $mailer, UserRepository $userRepository)
    {
        $lawyers = $userRepository->findAll();
        foreach($lawyers as $lawyer){
            if(in_array('ROLE_APPROVER', $lawyer->getRoles()) and in_array('ROLE_LAWYER', $lawyer->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($lawyer->getEmail())
                ->subject('Įkeltas naujas išėjimo iš darbo prašymas')
                ->html('<p>Įkeltas naujas išėjimo iš darbo prašymas.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/moderator", name="send_email_resignation_moderator")
     */
    public function sendEmailResignationRequestModerator(MailerInterface $mailer, UserRepository $userRepository)
    {
        $moderators = $userRepository->findAll();
        foreach($moderators as $moderator){
            if(in_array('ROLE_APPROVER', $moderator->getRoles()) and in_array('ROLE_ADMIN', $moderator->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($moderator->getEmail())
                ->subject('Įkeltas naujas išėjimo iš darbo prašymas')
                ->html('<p>Įkeltas naujas išėjimo iš darbo prašymas.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/manager/{id}", name="send_email_resignation_manager")
     */
    public function sendEmailResignationRequestManager(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $managers = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($managers as $manager){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($manager->getEmail())
            ->subject('Įkeltas naujas išėjimo iš darbo prašymas')
            ->html('<p>Įkeltas naujas išėjimo iš darbo prašymas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/approved/{id}", name="send_email_resignation_approved")
     */
    public function sendEmailResignationRequestApproved(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų išėjimo iš darbo prašymas patvirtintas')
            ->html('<p>Jūsų išėjimo iš darbo prašymas patvirtintas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/resigantion-request/rejected/{id}", name="send_email_resignation_rejected")
     */
    public function sendEmailResignationRequestRejected(MailerInterface $mailer, UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findBy(['id' => $request->get('id')]);
        foreach($users as $user){
            $email = (new Email())
            ->from('zivis.maileris@gmail.com')
            ->to($user->getEmail())
            ->subject('Jūsų išėjimo iš darbo prašymas atmestas')
            ->html('<p>Jūsų išėjimo iš darbo prašymas atmestas.');
            $mailer->send($email);
        }
        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Route("/email/anonymous-complaint", name="send_email_anonymous_complaint")
     */
    public function sendEmailAnonymousComplaint(MailerInterface $mailer, UserRepository $userRepository)
    {
        $moderators = $userRepository->findAll();
        foreach($moderators as $moderator){
            if(in_array('ROLE_MODERATOR', $moderator->getRoles())){
                $email = (new Email())
                ->from('zivis.maileris@gmail.com')
                ->to($moderator->getEmail())
                ->subject('Įkeltas naujas anoniminis skundas')
                ->html('<p>Įkeltas naujas anoniminis skundas.');
                $mailer->send($email);
            }
        }
        return $this->redirectToRoute('anonymous_complaint_new');
    }
}