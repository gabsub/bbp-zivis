<?php

namespace App\Controller;

use App\Entity\ResignationRequest;
use App\Form\ResignationRequestType;
use App\Repository\ResignationRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/resignation-request")
 */
class ResignationRequestController extends AbstractController
{
    /**
     * @Route("/new", name="resignation_request_new", methods={"GET","POST"})
     */
    public function new(Request $request, ResignationRequestRepository $resignationRequestRepository): Response
    {
        $resignationRequest = new ResignationRequest();
        $form = $this->createForm(ResignationRequestType::class, $resignationRequest);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $accepted_resignation_requests = $resignationRequestRepository->findBy(['status' => 1, 'submitter' => $this->getUser()->getId()]);
            if (!empty($accepted_resignation_requests)){
                $this->addFlash(
                    'negalima',
                    'Jūs jau turite patvirtintą išėjimo iš darbo prašymą'
                );
                return $this->redirectToRoute('resignation_request_new');
            }
            $accepted_resignation_requests = $resignationRequestRepository->findBy(['status' => 0, 'submitter' => $this->getUser()->getId()]);
            if (!empty($accepted_resignation_requests)){
                $this->addFlash(
                    'negalima',
                    'Jūs jau turite pateiktą išėjimo iš darbo prašymą'
                );
                return $this->redirectToRoute('resignation_request_new');
            }
            $resignationRequest->setSubmitter($this->getUser());
            $resignationRequest->setSubmittedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resignationRequest);
            $entityManager->flush();
            return $this->redirectToRoute('send_email_resignation_it');
        }

        return $this->render('resignation_request/new.html.twig', [
            'resignation_request' => $resignationRequest,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="resignation_request_show", methods={"GET"})
     */
    public function show(ResignationRequest $resignationRequest): Response
    {
        return $this->render('resignation_request/show.html.twig', [
            'resignation_request' => $resignationRequest,
        ]);
    }

    /**
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_APPROVER')")
     * @Route("/approve/{id}", name="resignation_request_approve")
     */
    public function approve(Request $request, ResignationRequest $resignationRequest): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        if(in_array('ROLE_MANAGER', $this->getUser()->getRoles()) and $resignationRequest->getSubmitter()->getManager()->getId() == $this->getUser()->getId()){
            $resignationRequest->setStatusByManager(ResignationRequest::STATUS_APPROVED);
            $resignationRequest->setApproverManager($this->getUser());
            $resignationRequest->setApprovedByManagerAt(new \DateTime);
        }
        if(in_array('ROLE_APPROVER', $this->getUser()->getRoles())){
            if(in_array('ROLE_LAWYER', $this->getUser()->getRoles())){
                $resignationRequest->setStatusByLawyer(ResignationRequest::STATUS_APPROVED);
                $resignationRequest->setApproverLawyer($this->getUser());
                $resignationRequest->setApprovedByLawyerAt(new \DateTime);
            }
            elseif(in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByITadmin(ResignationRequest::STATUS_APPROVED);
                $resignationRequest->setApproverITadmin($this->getUser());
                $resignationRequest->setApprovedByITadminAt(new \DateTime);
            }
            elseif(in_array('ROLE_ACCOUNTANT', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByAccountant(ResignationRequest::STATUS_APPROVED);
                $resignationRequest->setApproverAccountant($this->getUser());
                $resignationRequest->setApprovedByAccountantAt(new \DateTime);
            }
            elseif(in_array('ROLE_MODERATOR', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByAdministrator(ResignationRequest::STATUS_APPROVED);
                $resignationRequest->setApproverAdministrator($this->getUser());
                $resignationRequest->setApprovedByAdministratorAt(new \DateTime);
            }
        }
        if($resignationRequest->getStatusByManager() == ResignationRequest::STATUS_APPROVED and $resignationRequest->getStatusByLawyer() == ResignationRequest::STATUS_APPROVED and $resignationRequest->getStatusByITadmin() == ResignationRequest::STATUS_APPROVED and $resignationRequest->getStatusByAccountant() == ResignationRequest::STATUS_APPROVED and $resignationRequest->getStatusByAdministrator() ==ResignationRequest::STATUS_APPROVED){
            $resignationRequest->setStatus(ResignationRequest::STATUS_APPROVED);
        }
        $entityManager->persist($resignationRequest);
        $entityManager->flush();
        if($resignationRequest->getStatusByManager() == ResignationRequest::STATUS_APPROVED){
            return $this->redirectToRoute('send_email_resignation_approved', ['id' => $resignationRequest->getSubmitter()->getId()]);
        } elseif($resignationRequest->getStatusByAdministrator() == ResignationRequest::STATUS_APPROVED){
            return $this->redirectToRoute('send_email_resignation_manager', ['id' => $resignationRequest->getSubmitter()->getManager()->getId()]);
        } elseif($resignationRequest->getStatusByLawyer() == ResignationRequest::STATUS_APPROVED){
            return $this->redirectToRoute('send_email_resignation_moderator');
        } elseif($resignationRequest->getStatusByAccountant() == ResignationRequest::STATUS_APPROVED){
            return $this->redirectToRoute('send_email_resignation_lawyer');
        } elseif($resignationRequest->getStatusByITadmin() == ResignationRequest::STATUS_APPROVED){
            return $this->redirectToRoute('send_email_resignation_accountant');
        } else {
            return $this->redirectToRoute('request_for_approval');
        }
    }

    /**
     * @Security("is_granted('ROLE_MANAGER') or is_granted('ROLE_APPROVER')")
     * @Route("/reject/{id}", name="resignation_request_reject")
     */
    public function reject(Request $request, ResignationRequest $resignationRequest): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        if(in_array('ROLE_MANAGER', $this->getUser()->getRoles()) and $resignationRequest->getSubmitter()->getManager()->getId() == $this->getUser()->getId()){
            $resignationRequest->setStatusByManager(ResignationRequest::STATUS_REJECTED);
            $resignationRequest->setApproverManager($this->getUser());
            $resignationRequest->setApprovedByManagerAt(new \DateTime);
        }
        if(in_array('ROLE_APPROVER', $this->getUser()->getRoles())){
            if(in_array('ROLE_LAWYER', $this->getUser()->getRoles())){
                $resignationRequest->setStatusByLawyer(ResignationRequest::STATUS_REJECTED);
                $resignationRequest->setApproverLawyer($this->getUser());
                $resignationRequest->setApprovedByLawyerAt(new \DateTime);
            }
            elseif(in_array('ROLE_ADMIN', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByITadmin(ResignationRequest::STATUS_REJECTED);
                $resignationRequest->setApproverITadmin($this->getUser());
                $resignationRequest->setApprovedByITadminAt(new \DateTime);
            }
            elseif(in_array('ROLE_ACCOUNTANT', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByAccountant(ResignationRequest::STATUS_REJECTED);
                $resignationRequest->setApproverAccountant($this->getUser());
                $resignationRequest->setApprovedByAccountantAt(new \DateTime);
            }
            elseif(in_array('ROLE_MODERATOR', $this->getUser()->getRoles())) {
                $resignationRequest->setStatusByAdministrator(ResignationRequest::STATUS_REJECTED);
                $resignationRequest->setApproverAdministrator($this->getUser());
                $resignationRequest->setApprovedByAdministratorAt(new \DateTime);
            }
        }
        if($resignationRequest->getStatusByManager() == ResignationRequest::STATUS_REJECTED or $resignationRequest->getStatusByLawyer() == ResignationRequest::STATUS_REJECTED or $resignationRequest->getStatusByITadmin() == ResignationRequest::STATUS_REJECTED or $resignationRequest->getStatusByAccountant() == ResignationRequest::STATUS_REJECTED or $resignationRequest->getStatusByAdministrator() ==ResignationRequest::STATUS_REJECTED){
            $resignationRequest->setStatus(ResignationRequest::STATUS_REJECTED);
        }
        $entityManager->persist($resignationRequest);
        $entityManager->flush();
        if($resignationRequest->getStatus() == ResignationRequest::STATUS_REJECTED){
            return $this->redirectToRoute('send_email_resignation_rejected', ['id' => $resignationRequest->getSubmitter()->getId()]);
        } else {
            return $this->redirectToRoute('request_for_approval');
        }
    }
}
