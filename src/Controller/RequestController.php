<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\ResignationRequest;
use App\Repository\ResignationRequestRepository;
use App\Entity\InvoiceRefundRequest;
use App\Repository\InvoiceRefundRequestRepository;
use App\Entity\VacationRequest;
use App\Repository\VacationRequestRepository;
use App\Entity\User;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/request")
 */
class RequestController extends AbstractController
{
    public const STATUS_SUBMITTED = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_SUBMITTED_TEXT = 'Pateikta';
    public const STATUS_APPROVED_TEXT = 'Patvirtinta';
    public const STATUS_REJECTED_TEXT = 'Atmesta';

    /**
     * @Route("/", name="request_index")
     */
    public function index(ResignationRequestRepository $resignationRequestRepository, InvoiceRefundRequestRepository $invoiceRefundRequestRepository, VacationRequestRepository $vacationRequestRepository): Response
    {
        $resignationRequestsByID = $resignationRequestRepository->findBySubmitter($this->getUser()->getId());
        $invoiceRefundRequestsByID = $invoiceRefundRequestRepository->findBySubmitter($this->getUser()->getId());
        $vacationRequestByID = $vacationRequestRepository->findBySubmitter($this->getUser()->getId());
        $requestsArray = array();
        foreach($resignationRequestsByID as $request){
            $requestsArray[$request->getSubmittedAt()->format('Y-m-d H:i:s')] = array('resignation' => $request);
        }
        foreach($invoiceRefundRequestsByID as $request){
            $requestsArray[$request->getCreatedAt()->format('Y-m-d H:i:s')] = array('invoice_refund' => $request);
        }
        foreach($vacationRequestByID as $request){
            $requestsArray[$request->getCreatedAt()->format('Y-m-d H:i:s')] = array('vacation' => $request);
        }
        krsort($requestsArray);
        return $this->render('request/index.html.twig', [
            'requests' => $requestsArray,
        ]);
    }

    /**
     * @Route("/approval", name="request_for_approval")
     */
    public function approval(ResignationRequestRepository $resignationRequestRepository, InvoiceRefundRequestRepository $invoiceRefundRequestRepository, VacationRequestRepository $vacationRequestRepository, UserRepository $userRepository): Response
    {
        $requestsArray = array();
        if($this->isGranted('ROLE_ACCOUNTANT')){
            $invoiceRefundRequestsForApprove = $invoiceRefundRequestRepository->findAll();
            foreach($invoiceRefundRequestsForApprove as $request){
                $requestsArray[$request->getCreatedAt()->format('Y-m-d H:i:s')] = array('invoice_refund' => $request);
            }
        }
        if($this->isGranted('ROLE_MANAGER')){
            $subordinates = $userRepository->findSubordinatesByManagerId($this->getUser()->getId());
            foreach($subordinates as $subordinate){
                $resignationRequestsForApprove = $resignationRequestRepository->findBySubmitter($subordinate->getId());
                foreach($resignationRequestsForApprove as $request){
                    $requestsArray[$request->getSubmittedAt()->format('Y-m-d H:i:s')] = array('resignation' => $request);
                }
                $vacationRequestForApprove = $vacationRequestRepository->findBySubmitter($subordinate->getId());
                foreach($vacationRequestForApprove as $request){
                    $requestsArray[$request->getCreatedAt()->format('Y-m-d H:i:s')] = array('vacation' => $request);
                }
            }
        }
        if($this->isGranted('ROLE_APPROVER')){
            $resignationRequestsForApprove = $resignationRequestRepository->findAll();
            foreach($resignationRequestsForApprove as $request){
                $requestsArray[$request->getSubmittedAt()->format('Y-m-d H:i:s')] = array('resignation' => $request);
            }
        }
        krsort($requestsArray);
        if(count($requestsArray) >= 0){
            return $this->render('request/approval.html.twig', [
                'requests' => $requestsArray,
            ]);
        } else {
            return $this->render('\bundles\TwigBundle\Exception\error403.html.twig');
        }
    }

    /**
     * @Route("/calendar", name="request_calendar")
     */
    public function calendar(ResignationRequestRepository $resignationRequestRepository, VacationRequestRepository $vacationRequestRepository, UserRepository $userRepository): Response
    {
        if(!empty($_POST)){
            $date = explode('-', $_POST['date']);
            $year = $date[0];
            $month = $date[1];
            
        } else {
            $today = new \DateTime();
            $year = date_format($today, 'Y');
            $month = date_format($today, 'm');
        }
        $user = $this->getUser();
        if($this->getUser()->getManager() == null){
            $group1 = $userRepository->findBy(['id' => $this->getUser()->getId()]);
        } else {
            $group1 = $userRepository->findGroup($this->getUser()->getManager()->getId());
        }
        $group2 = $userRepository->findBy(['manager' => $this->getUser()->getId()]);
        $days1 = array();
        $users1 = array();
        $days2 = array();
        $users2 = array();
        $date1 = $year . '-' . $month . '-' . '01';
        if($month != 12){
            $date2 = $year . '-' . ((int)$month + 1) . '-' . '01';
        } else {
            $date2 = $year+1 . '-' . 1 . '-' . '01';
        }
        $biggerThan = new \DateTime($date1);
        $biggerThan = date_format($biggerThan, 'Y-m-d H:i:s');
        $smallerThan = new \DateTime($date2);
        $smallerThan = date_format($smallerThan, 'Y-m-d H:i:s');
        foreach($group1 as $member){
            $vacationRequests = $vacationRequestRepository->findByDateAndSubmitter($member->getId(), $smallerThan, $biggerThan);
            $vacationDays = array();
            foreach($vacationRequests as $request){
                if((int)$request->getFinishesAt()->format('m') != (int)$request->getStartsAt()->format('m')){
                    if((int)$request->getFinishesAt()->format('m') == (int)$month){
                        $start = 1;
                        $end = (int)$request->getFinishesAt()->format('d');
                    } elseif ((int)$request->getStartsAt()->format('m') == (int)$month) {
                        $start = (int)$request->getStartsAt()->format('d');
                        $end = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                    } else {
                        $start = 1;
                        $end = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                    }
                } else {
                    $start = (int)$request->getStartsAt()->format('d');
                    $end = (int)$request->getFinishesAt()->format('d');
                }
                for($i = $start; $i <= $end; $i++){
                    $vacationDays[$i] = 1;
                }
            }
            $days1[$member->getId()] = $vacationDays;
            $users1[$member->getId()] = $member;
        }

        foreach($group2 as $member){
            $vacationRequests = $vacationRequestRepository->findByDateAndSubmitter($member->getId(), $smallerThan, $biggerThan);
            $vacationDays = array();
            foreach($vacationRequests as $request){
                if((int)$request->getFinishesAt()->format('m') != (int)$request->getStartsAt()->format('m')){
                    if((int)$request->getFinishesAt()->format('m') == (int)$month){
                        $start = 1;
                        $end = (int)$request->getFinishesAt()->format('d');
                    } elseif ((int)$request->getStartsAt()->format('m') == (int)$month) {
                        $start = (int)$request->getStartsAt()->format('d');
                        $end = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                    } else {
                        $start = 1;
                        $end = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                    }
                } else {
                    $start = (int)$request->getStartsAt()->format('d');
                    $end = (int)$request->getFinishesAt()->format('d');
                }
                for($i = $start; $i <= $end; $i++){
                    $vacationDays[$i] = 1;
                }
            }
            $days2[$member->getId()] = $vacationDays;
            $users2[$member->getId()] = $member;
        }

        $weekends = array();
        for($i = 1; $i <= cal_days_in_month(CAL_GREGORIAN, $month, $year); $i++){
            if($i < 10){
                $dayOfMonth = $year . '-' . $month . '-0' . $i;
            } else {
                $dayOfMonth = $year . '-' . $month . '-' . $i;
            }
            $dayOfMonth = new \DateTime($dayOfMonth);
            if($dayOfMonth->format('w') == 6 or $dayOfMonth->format('w') == 0){
                $weekends[$i] = 1;
            } elseif (($month == '01' and $i == 1) or ($month == '02' and $i == 16) or ($month == '03' and $i == 11) or ($month == '05' and $i == 1) or ($month == '06' and $i == 24) or ($month == '07' and $i == 6) or ($month == '08' and $i == 15) or ($month == '11' and $i == 1) or ($month == '11' and $i == 2) or ($month == '12' and $i == 24) or ($month == '12' and $i == 25) or ($month == '12' and $i == 26)) {
                $weekends[$i] = 1;
            } else {
                $weekends[$i] = 0;
            }
        }
        return $this->render('request/calendar.html.twig', [
            'year' => $year,
            'month' => $month,
            'days1' => $days1,
            'users1' => $users1,
            'days2' => $days2,
            'users2' => $users2,
            'number' => cal_days_in_month(CAL_GREGORIAN, $month, $year),
            'weekends' => $weekends,
        ]);
    }
}