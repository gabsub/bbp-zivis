<?php

namespace App\Controller;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Repository\FeedbackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\FeedbackQuestionRepository;

/**
 * @Route("/feedback")
 */
class FeedbackController extends AbstractController
{
    /**
     * @Route("/", name="feedback_index", methods={"GET"})
     */
    public function index(FeedbackRepository $feedbackRepository): Response
    {
        $feedbacks = array();
        $allFeedbacks = $feedbackRepository->findAll();
        foreach($allFeedbacks as $feedback){
            if($feedback->getTarget()->getManager()->getId() == $this->getUser()->getId()){
                array_push($feedbacks, $feedback);
            }
        }
        return $this->render('feedback/index.html.twig', [
            'feedback' => $feedbacks,
        ]);
    }

    //TODO: matoma tik nuo gruodzio 1 iki sausio 1
    /**
     * @Route("/new", name="feedback_new", methods={"GET","POST"})
     */
    public function new(Request $request, FeedbackQuestionRepository $feedback_question_rep): Response
    {
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);
        $form->handleRequest($request);

        $questions = $feedback_question_rep->findByActiveStatus(1);

        foreach($questions as $question){

            if($question->getAnswerType() == 'bool'){
                $form_field = '<br><input class="form-check-input" type="checkbox" id="'.$question->getId().'" name="'.$question->getId().'" value="0"><br>';
            }
            elseif($question->getAnswerType() == 'integer'){
                $form_field = '<input class="form-control" type="number" id="'.$question->getId().'" name="'.$question->getId().'" min="1" max="5" value="5">';
            }
            else{
                $form_field = '<input class="form-control" type="text" id="'.$question->getId().'" name="'.$question->getId().'" required>';
            }

            $question->field = $form_field;
        }


        if ($form->isSubmitted() && $form->isValid() && !empty($_POST)) {
            $answer = $_POST;
            $answerArray = array();
            $count = 1;
            foreach($feedback_question_rep->findByActiveStatus(1) as $question){
                if(isset($answer[$question->getId()])){
                    $answerArray[$count] = [$question->getQuestion() => [$answer[$question->getId()] => $question->getAnswerType()]];
                    $count++;
                }
                elseif($question->getAnswerType() == 'bool'){
                    $answerArray[$count] = [$question->getQuestion() => [(string)0 => 'bool']];
                    $count++;
                }
            }
            $feedback->setAnswer($answerArray);
            $feedback->setSubmitter($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($feedback);
            $entityManager->flush();
            $this->addFlash(
                'pavyko',
                'Jūsų atsiliepimas išsaugotas'
            );
            return $this->redirectToRoute('feedback_new');
        }

        return $this->render('feedback/new.html.twig', [
            'feedback' => $feedback,
            'form' => $form->createView(),
            'questions' => $questions
        ]);
    }

    /**
     * @Route("/{id}", name="feedback_show", methods={"GET"})
     */
    public function show(Feedback $feedback): Response
    {
        return $this->render('feedback/show.html.twig', [
            'feedback' => $feedback,
        ]);
    }
}
