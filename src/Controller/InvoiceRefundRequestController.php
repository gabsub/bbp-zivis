<?php

namespace App\Controller;

use App\Entity\InvoiceRefundRequest;
use App\Form\InvoiceRefundRequestType;
use App\Repository\InvoiceRefundRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/invoice-refund-request")
 */
class InvoiceRefundRequestController extends AbstractController
{
    /**
     * @Route("/new", name="invoice_refund_request_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $invoiceRefundRequest = new InvoiceRefundRequest();
        $form = $this->createForm(InvoiceRefundRequestType::class, $invoiceRefundRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if($form->isValid()){
                $file = $form->get('invoiceFileName')->getData();
                if ($file) {
                    $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                    $safeFilename = $slugger->slug($originalFilename);
                    $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
                    try {
                        $file->move(
                            $this->getParameter('invoices_directory'),
                            $newFilename
                        );
                    } catch (FileException $e) { }
                    $invoiceRefundRequest->setInvoiceFileName($newFilename);
                }

                $invoiceRefundRequest->setCreatedAt(new \DateTime());
                $invoiceRefundRequest->setSubmitter($this->getUser());

                if($invoiceRefundRequest->getRefundAmount() > 0){
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($invoiceRefundRequest);
                    $entityManager->flush();
                    return $this->redirectToRoute('send_email_invoice_refund');
                } else {
                    $this->addFlash(
                        'refundAmount',
                        'Grąžintina suma turi būti didesnė už 0'
                    );
                    return $this->redirectToRoute('invoice_refund_request_new');
                }
            } else {
                $this->addFlash(
                    'file',
                    'Failo tipas turi būti .pdf formato, o dydis neviršyti 4MB'
                );
                return $this->redirectToRoute('invoice_refund_request_new');
            }
        }

        return $this->render('invoice_refund_request/new.html.twig', [
            'invoice_refund_request' => $invoiceRefundRequest,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="invoice_refund_request_show", methods={"GET"})
     */
    public function show(InvoiceRefundRequest $invoiceRefundRequest): Response
    {
       
        return $this->render('invoice_refund_request/show.html.twig', [
            'invoice_refund_request' => $invoiceRefundRequest,
        ]);
    }

    /**
     * @Route("/show-document/{id}", name="invoice_document_show", methods={"GET"})
     */
    public function showDocument(InvoiceRefundRequest $invoiceRefundRequest): Response
    {
        $filename = $invoiceRefundRequest->getInvoiceFileName();
        return new BinaryFileResponse($this->getParameter('invoices_directory').'/'.$filename);
       
    }

    /**
     * @Security("is_granted('ROLE_ACCOUNTANT')")
     * @Route("/approve/{id}", name="invoice_refund_request_approve")
     */
    public function approve(Request $request, InvoiceRefundRequest $invoiceRefundRequest): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invoiceRefundRequest->setStatus(InvoiceRefundRequest::STATUS_APPROVED);
        $invoiceRefundRequest->setApprover($this->getUser());
        $invoiceRefundRequest->setAcceptedAt(new \DateTime);
        $entityManager->persist($invoiceRefundRequest);
        $entityManager->flush();
        return $this->redirectToRoute('send_email_invoice_refund_approved', ['id' => $invoiceRefundRequest->getSubmitter()->getId()]);
    }

    /**
     * @Security("is_granted('ROLE_ACCOUNTANT')")
     * @Route("/reject/{id}", name="invoice_refund_request_reject")
     */
    public function reject(Request $request, InvoiceRefundRequest $invoiceRefundRequest): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $invoiceRefundRequest->setStatus(InvoiceRefundRequest::STATUS_REJECTED);
        $invoiceRefundRequest->setApprover($this->getUser());
        $invoiceRefundRequest->setAcceptedAt(new \DateTime);
        $entityManager->persist($invoiceRefundRequest);
        $entityManager->flush();
        return $this->redirectToRoute('send_email_invoice_refund_rejected', ['id' => $invoiceRefundRequest->getSubmitter()->getId()]);
    }
}
