<?php

namespace App\Controller;

use App\Entity\AnonymousComplaint;
use App\Form\AnonymousComplaintType;
use App\Repository\AnonymousComplaintRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/anonymous-complaint")
 */
class AnonymousComplaintController extends AbstractController
{   
    /**
     * @Security("is_granted('ROLE_MODERATOR')")
     * @Route("/", name="anonymous_complaint_index", methods={"GET"})
     */
    public function index(AnonymousComplaintRepository $anonymousComplaintRepository): Response
    {
        return $this->render('anonymous_complaint/index.html.twig', [
            'anonymous_complaints' => $anonymousComplaintRepository->findBy(array(), ['createdAt' => 'ASC']),
        ]);
    }

    /**
     * @Route("/new", name="anonymous_complaint_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $anonymousComplaint = new AnonymousComplaint();
        $form = $this->createForm(AnonymousComplaintType::class, $anonymousComplaint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $anonymousComplaint->setCreatedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anonymousComplaint);
            $entityManager->flush();
            $this->addFlash(
                'pavyko',
                'Jūsų skundas išsaugotas'
            );
            return $this->redirectToRoute('send_email_anonymous_complaint');
        }

        return $this->render('anonymous_complaint/new.html.twig', [
            'anonymous_complaint' => $anonymousComplaint,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_MODERATOR')")
     * @Route("/{id}", name="anonymous_complaint_show", methods={"GET"})
     */
    public function show(AnonymousComplaint $anonymousComplaint): Response
    {
        return $this->render('anonymous_complaint/show.html.twig', [
            'anonymous_complaint' => $anonymousComplaint,
        ]);
    }
}
