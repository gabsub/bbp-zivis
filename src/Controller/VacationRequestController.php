<?php

namespace App\Controller;

use App\Entity\VacationRequest;
use App\Form\VacationRequestType;
use App\Repository\VacationRequestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\UserRepository;
/**
 * @Security("is_granted('ROLE_USER')")
 * @Route("/vacation-request")
 */
class VacationRequestController extends AbstractController
{
    /**
     * @Route("/new", name="vacation_request_new", methods={"GET","POST"})
     */
    public function new(Request $request, VacationRequestRepository $vacation_request_rep): Response
    {     
        $now = new \DateTime();
        $diff = $now->diff($this->getUser()->getDateOfEmployment())->format("%a");
        $overall_vacation_days = (int)floor($diff*(20/365));

        $vacation_requests = $vacation_request_rep->findBy(["status" => 1, 'submitter' => $this->getUser(), 'vacationType' => 1]);

        $used_days = 0;
        foreach($vacation_requests as $vacation_request){
            $start = $vacation_request->getStartsAt();
            $end   = $vacation_request->getFinishesAt();
            $businessDays = 0;
            while ($start <= $end){
                if (! in_array($start->format('w'), [0, 6])) {
                    if (($start->format('m') == '01' and $start->format('j') == 1) or ($start->format('m') == '02' and $start->format('j') == 16) or ($start->format('m') == '03' and $start->format('j') == 11) or ($start->format('m') == '05' and $start->format('j') == 1) or ($start->format('m') == '06' and $start->format('j') == 24) or ($start->format('m') == '07' and $start->format('j') == 6) or ($start->format('m') == '08' and $start->format('j') == 15) or ($start->format('m') == '11' and $start->format('j') == 1) or ($start->format('m') == '11' and $start->format('j') == 2) or ($start->format('m') == '12' and $start->format('j') == 24) or ($start->format('m') == '12' and $start->format('j') == 25) or ($start->format('m') == '12' and $start->format('j') == 26)) {
                
                    } else {
                        $businessDays++;
                    }
                }
                $start->add(new \DateInterval('P1D'));
            }
            $used_days+=$businessDays;
        }

        $vacation_requests1 = $vacation_request_rep->findBy(["status" => 0, 'submitter' => $this->getUser(), 'vacationType' => 1]);
        foreach($vacation_requests1 as $vacation_request){
            $start = $vacation_request->getStartsAt();
            $end   = $vacation_request->getFinishesAt();
            $businessDays = 0;
            while ($start <= $end){
                if (! in_array($start->format('w'), [0, 6])) {
                    if (($start->format('m') == '01' and $start->format('j') == 1) or ($start->format('m') == '02' and $start->format('j') == 16) or ($start->format('m') == '03' and $start->format('j') == 11) or ($start->format('m') == '05' and $start->format('j') == 1) or ($start->format('m') == '06' and $start->format('j') == 24) or ($start->format('m') == '07' and $start->format('j') == 6) or ($start->format('m') == '08' and $start->format('j') == 15) or ($start->format('m') == '11' and $start->format('j') == 1) or ($start->format('m') == '11' and $start->format('j') == 2) or ($start->format('m') == '12' and $start->format('j') == 24) or ($start->format('m') == '12' and $start->format('j') == 25) or ($start->format('m') == '12' and $start->format('j') == 26)) {
                
                    } else {
                        $businessDays++;
                    }
                }
                $start->add(new \DateInterval('P1D'));
            }
            $used_days+=$businessDays;
        }

        $available_days = $overall_vacation_days - $used_days;

        $vacationRequest = new VacationRequest();
        $form = $this->createForm(VacationRequestType::class, $vacationRequest);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $substitute_vacation_requests = $vacation_request_rep->findByDateAndSubmitter($vacationRequest->getSubstitute()->getId(), $vacationRequest->getFinishesAt(), $vacationRequest->getStartsAt());
            if(!empty($substitute_vacation_requests)){
                $this->addFlash(
                    'substitute',
                    'Pasirinktomis dienomis ' . $vacationRequest->getSubstitute()->getName() . ' ' . $vacationRequest->getSubstitute()->getLastName() . ' Jūsų pavaduoti negali.'
                );
                return $this->redirectToRoute('vacation_request_new');
            }
            if($vacationRequest->getVacationType()->getId() == 1){
                $start = new \DateTime($vacationRequest->getStartsAt()->format('Y-m-d'));
                $end   = $vacationRequest->getFinishesAt();
                $businessDaysUsed = 0;
                while ($start <= $end){
                    if (! in_array($start->format('w'), [0, 6])) {
                        if (($start->format('m') == '01' and $start->format('j') == 1) or ($start->format('m') == '02' and $start->format('j') == 16) or ($start->format('m') == '03' and $start->format('j') == 11) or ($start->format('m') == '05' and $start->format('j') == 1) or ($start->format('m') == '06' and $start->format('j') == 24) or ($start->format('m') == '07' and $start->format('j') == 6) or ($start->format('m') == '08' and $start->format('j') == 15) or ($start->format('m') == '11' and $start->format('j') == 1) or ($start->format('m') == '11' and $start->format('j') == 2) or ($start->format('m') == '12' and $start->format('j') == 24) or ($start->format('m') == '12' and $start->format('j') == 25) or ($start->format('m') == '12' and $start->format('j') == 26)) {
                    
                        } else {
                            $businessDaysUsed++;
                        }
                    }
                    $start->add(new \DateInterval('P1D'));
                }
                if($businessDaysUsed > $available_days){
                    $this->addFlash(
                        'max_bussines_days',
                        'Jūs pasirinkote per daug apmokamų atostogų darbo dienų.'
                    );
                    return $this->redirectToRoute('vacation_request_new');
                }
            }
            $vacationRequest->setCreatedAt(new \DateTime());
            $vacationRequest->setSubmitter($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($vacationRequest);
            $entityManager->flush();
            return $this->redirectToRoute('send_email_vacation');
        }
        return $this->render('vacation_request/new.html.twig', [
            'vacation_request' => $vacationRequest,
            'form' => $form->createView(),
            'available_days' => $available_days,
        ]);
    }

    /**
     * @Route("/{id}", name="vacation_request_show", methods={"GET"})
     */
    public function show(VacationRequest $vacationRequest): Response
    {
        return $this->render('vacation_request/show.html.twig', [
            'vacation_request' => $vacationRequest,
        ]);
    }
    
    /**
     * @Security("is_granted('ROLE_MANAGER')")
     * @Route("/approve/{id}", name="vacation_request_approve")
     */
    public function approve(Request $request, VacationRequest $vacationRequest, UserRepository $userRepository): Response
    {
        $managerOfRequestSubmitter = $userRepository->findOneBy(['id' => $vacationRequest->getSubmitter()->getId()])->getManager()->getId();
        if ($this->getUser()->getId() == $managerOfRequestSubmitter){
            $entityManager = $this->getDoctrine()->getManager();
            $vacationRequest->setStatus(VacationRequest::STATUS_APPROVED);
            $vacationRequest->setApprover($this->getUser());
            $vacationRequest->setAcceptedAt(new \DateTime);
            $entityManager->persist($vacationRequest);
            $entityManager->flush();
            return $this->redirectToRoute('send_email_resignation_approved', ['id' => $vacationRequest->getSubmitter()->getId()]);
        }

        return $this->redirectToRoute('request_for_approval');
    }

    /**
     * @Security("is_granted('ROLE_MANAGER')")
     * @Route("/reject/{id}", name="vacation_request_reject")
     */
    public function reject(Request $request, VacationRequest $vacationRequest, UserRepository $userRepository): Response
    {
        $managerOfRequestSubmitter = $userRepository->findOneBy(['id' => $vacationRequest->getSubmitter()->getId()])->getManager()->getId();
        if ($this->getUser()->getId() == $managerOfRequestSubmitter){
            $entityManager = $this->getDoctrine()->getManager();
            $vacationRequest->setStatus(VacationRequest::STATUS_REJECTED);
            $vacationRequest->setApprover($this->getUser());
            $vacationRequest->setAcceptedAt(new \DateTime);
            $entityManager->persist($vacationRequest);
            $entityManager->flush();
            return $this->redirectToRoute('send_email_resignation_rejected', ['id' => $vacationRequest->getSubmitter()->getId()]);
        }

        return $this->redirectToRoute('request_for_approval');
    }
}
